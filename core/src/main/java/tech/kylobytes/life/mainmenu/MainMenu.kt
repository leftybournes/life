/*
 * Copyright (c) 2020  Kent Delante <kdeleteme@tutanota.com>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the “Software”),
 * to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 */

package tech.kylobytes.life.mainmenu

import com.badlogic.gdx.Gdx
import com.badlogic.gdx.Input
import com.badlogic.gdx.graphics.Texture
import com.badlogic.gdx.scenes.scene2d.Action
import com.badlogic.gdx.scenes.scene2d.actions.Actions.*
import com.badlogic.gdx.scenes.scene2d.ui.Image
import com.badlogic.gdx.scenes.scene2d.ui.Label
import com.badlogic.gdx.utils.Align
import ktx.actors.alpha
import ktx.actors.then
import ktx.app.KtxScreen
import ktx.scene2d.label
import ktx.scene2d.scene2d
import ktx.scene2d.stack
import ktx.scene2d.table
import tech.kylobytes.life.Life
import tech.kylobytes.life.util.Asset.MAIN_MENU_PARTICLE
import kotlin.math.round

class MainMenu(private val game: Life) : KtxScreen {

    private val particleContainer by lazy {
        scene2d.table { setFillParent(true)  }
    }

    private var tapToPlay: Label
    private var exitConfirmNotice: Label
    private var accumulatedDelta = 0f
    private var backInitiallyPressed = false

    init {
        Gdx.input.inputProcessor = game.stage
        Gdx.input.setCatchKey(Input.Keys.BACK, true)
    }

    private val content = scene2d.stack {
        setFillParent(true)

        add(particleContainer)

        table {
            setFillParent(true)

            label(text = "Game of Life", style = "font-large")
                    .cell(row = true, expandX = true, padBottom = TITLE_BOTTOM_PADDING)
            tapToPlay = label(text = "Tap to Play", style = "font-regular")
                    .cell(row = true, expandX = true)
        }

        table {
            setFillParent(true)

            exitConfirmNotice = label(
                    "Tap back again to exit",
                    style = "font-regular"
            ).also {
                it.alpha = 0f
            }.cell(
                    row = true,
                    expand = true,
                    align = Align.bottom,
                    padBottom = 100f
            )
        }
    }

    private fun createParticle(): Image {
        val posx = (0..game.stage.camera.viewportWidth.toInt())
                .random()
                .toFloat()
        val posy = (0..game.stage.camera.viewportHeight.toInt())
                .random()
                .toFloat()
        val size = (0..MAX_PARTICLE_SIZE).random().toFloat()
        val particle = Image(game.assetStorage.get<Texture>(MAIN_MENU_PARTICLE))
                .also {
                    it.setPosition(posx, posy)
                    it.setSize(size, size)
                }

        val scaleAmount = 2f
        val scaleDuration = 1f
        val scaleAction = scaleBy(scaleAmount, scaleAmount, scaleDuration)

        val fadeOutDuration = 1f
        val fadeOutAction = fadeOut(fadeOutDuration)
        val actionSequence = scaleAction then fadeOutAction then
                removeActor()
        particle.addAction(actionSequence)

        return particle
    }

    private fun animateTapToPlay() {
        val actionDuration = 2f
        val blinkingSequence = forever(
                fadeOut(actionDuration) then
                        fadeIn(actionDuration)
        )
        tapToPlay.addAction(blinkingSequence)
    }

    private fun showConfirmExitNotice() {
        exitConfirmNotice.clearActions()

        val fadeInDuration = 2f
        val fadeOutDuration = 5f
        val resetBackPressed = object: Action() {
            override fun act(delta: Float): Boolean {
                backInitiallyPressed = false
                return true
            }
        }
        val sequence = fadeIn(fadeInDuration) then fadeOut(fadeOutDuration) then
                resetBackPressed
        exitConfirmNotice.addAction(sequence)
    }

    override fun show() {
        game.stage.addActor(content)
        animateTapToPlay()
    }

    override fun render(delta: Float) {
        if (round(accumulatedDelta) > 0) {
            accumulatedDelta = 0f
            val particle = createParticle()
            particleContainer.addActor(particle)
        }

        accumulatedDelta += delta

        if (Gdx.input.isKeyJustPressed(Input.Keys.BACK)) {
            if (!backInitiallyPressed) {
                backInitiallyPressed = true
                showConfirmExitNotice()
            } else if(backInitiallyPressed && exitConfirmNotice.hasActions()) {
                Gdx.app.exit()
            }
        }

        game.stage.apply {
            act(delta)
            draw()
        }
    }

    override fun resize(width: Int, height: Int) {
        game.stage.viewport.update(width, height)
    }

    override fun dispose() {
        game.stage.dispose()
        super.dispose()
    }

    companion object {
        const val MAX_PARTICLE_SIZE = 100
        const val TITLE_BOTTOM_PADDING = 40f
    }
}