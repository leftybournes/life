package tech.kylobytes.life

import com.badlogic.gdx.graphics.Texture
import com.badlogic.gdx.graphics.g2d.SpriteBatch
import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGenerator
import com.badlogic.gdx.scenes.scene2d.Stage
import com.badlogic.gdx.utils.viewport.ScreenViewport
import kotlinx.coroutines.launch
import ktx.app.KtxGame
import ktx.app.KtxScreen
import ktx.assets.async.AssetStorage
import ktx.async.KtxAsync
import ktx.freetype.async.registerFreeTypeFontLoaders
import ktx.freetype.generateFont
import ktx.scene2d.Scene2DSkin
import ktx.style.label
import ktx.style.skin
import tech.kylobytes.life.game.GameScreen
import tech.kylobytes.life.mainmenu.MainMenu
import tech.kylobytes.life.util.Asset.COMFORTAA_TTF
import tech.kylobytes.life.util.Asset.MAIN_MENU_PARTICLE


/** [com.badlogic.gdx.ApplicationListener] implementation shared by all platforms.  */
class Life : KtxGame<KtxScreen>() {

    val assetStorage by lazy {
        AssetStorage()
    }

    val stage by lazy { Stage(ScreenViewport(), SpriteBatch()) }

    override fun create() {
        KtxAsync.initiate()
        KtxAsync.launch {
            assetStorage.apply {
                registerFreeTypeFontLoaders()

                val fontGenerator = load<FreeTypeFontGenerator>(COMFORTAA_TTF)

                val fontLarge = fontGenerator.generateFont {
                    size = 120
                }

                val fontRegular = fontGenerator.generateFont {
                    size = 48
                }

                Scene2DSkin.defaultSkin = skin {
                    label("font-large") {
                        font = fontLarge
                    }

                    label("font-regular") {
                        font = fontRegular
                    }
                }

                load<Texture>(MAIN_MENU_PARTICLE)

                addScreen(MainMenu(this@Life))
                addScreen(GameScreen())
                setScreen<MainMenu>()
            }
        }

    }

    override fun dispose() {
        assetStorage.dispose()

        super.dispose()
    }
}