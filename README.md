# Life

Conway's Game of Life implemented in [libgdx](https://libgdx.badlogicgames.com)

This is simply a project meant for me to learn libgdx and gamedev.

## License
See [COPYING](COPYING)
